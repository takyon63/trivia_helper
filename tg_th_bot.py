from __future__ import annotations
import logging
import collections
from operator import attrgetter
from typing import Iterable, List, Optional, Pattern, Tuple
import telebot
import wikipediaapi
import spacy
from telebot import types
from textacy.extract import matches
import textacy.types
import textacy.utils




from spacy.symbols import (
    AUX,
    VERB,
    attr,
    aux,
    auxpass,
    dobj,
    neg,
    nsubj,
    nsubjpass,
    obj,
    xcomp,
)
from spacy.tokens import Token


_NOMINAL_SUBJ_DEPS = {nsubj, nsubjpass}
_VERB_MODIFIER_DEPS = {aux, auxpass, neg}

SSSTriple: Tuple[List[Token], List[Token], List[Token]] = collections.namedtuple(
    "SSSTriple", ["entity", "cue", "fragment"]
)

BOT_TOKEN = ""
logging.basicConfig(
    level=logging.INFO,
    filename="log.log",
    filemode="w",
    format="%(asctime)s - %(levelname)s - %(message)s",
)

nlp = spacy.load("en_core_web_sm")
wiki_wiki = wikipediaapi.Wikipedia("en")
common_verbs = [
    "be",
    "have",
    "do",
    "say",
    "get",
    "make",
    "go",
    "know",
    "take",
    "see",
    "come",
    "think",
    "look",
    "want",
    "give",
    "use",
    "find",
    "tell",
    "ask",
    "work",
    "seem",
    "feel",
    "try",
    "leave",
    "call",
    "write",
]


def custom_semistructured_statements(
    doclike: types.DocLike,
    *,
    entity: str | Pattern,
    cue: str,
    fragment_len_range: Optional[Tuple[Optional[int], Optional[int]]] = None,
) -> Iterable[SSSTriple]:
    """Extract “semi-structured statements” from a document as a sequence of
    (entity, cue, fragment) triples with more information"""
    if fragment_len_range is not None:
        fragment_len_range = textacy.utils.validate_and_clip_range(
            fragment_len_range, (1, 1000), int
        )
    for entity_cand in matches.regex_matches(doclike, entity, alignment_mode="strict"):
        # is the entity candidate a nominal subject?
        if entity_cand.root.dep in _NOMINAL_SUBJ_DEPS:
            cue_cand = entity_cand.root.head
            # is the cue candidate a verb with matching lemma?
            if cue_cand.pos in {VERB, AUX} and cue_cand.lemma_ == cue:
                frag_cand = None

                for tok in cue_cand.children:
                    if (
                        tok.dep in {attr, dobj, obj}
                        or tok.dep_ == "dative"
                        or (
                            tok.dep == xcomp
                            and not any(
                                child.dep == dobj for child in cue_cand.children
                            )
                        )
                    ):
                        subtoks = list(tok.subtree)
                        for elem in iter(cue_cand.children):
                            if elem.dep_ == "prep":
                                subtoks = subtoks + list(elem.subtree)

                        if (
                            fragment_len_range is None
                            or fragment_len_range[0]
                            <= len(subtoks)
                            < fragment_len_range[1]
                        ):

                            frag_cand = subtoks
                            break

                if frag_cand is not None:
                    yield SSSTriple(
                        entity=list(entity_cand),
                        cue=sorted(expand_verb(cue_cand), key=attrgetter("i")),
                        fragment=sorted(frag_cand, key=attrgetter("i")),
                    )


def expand_verb(tok: Token) -> List[Token]:
    """Expand a verb token to include all associated auxiliary and negation tokens."""
    verb_modifiers = [
        child for child in tok.children if child.dep in _VERB_MODIFIER_DEPS
    ]
    return [tok] + verb_modifiers


def wiki_to_doc(message):
    '''function to parse text to the ready to extract facts class'''
    wiki_text_doc = nlp(wiki_wiki.page(message).text)
    return wiki_text_doc


def statements_to_sents(unique_statements):
    '''Function that changes SSSTriples to sentences'''
    str_statements = []
    for statement in unique_statements:
        entity, cue, fact = statement
        str_statement = str(entity).translate(str.maketrans(' ', ' ', ',[]')) + " " + \
             str(cue).translate(
            str.maketrans(' ', ' ', ',[]')) + " " + \
                str(fact).translate(str.maketrans(' ', ' ', ',[]')).rstrip()
        str_statements.append(str_statement)
    return str_statements


def answers(str_statements):
    '''Function that changes list of statements to list of
    list in order to bypass telegram message length restriction'''
    answers_list = []
    temp_list = []
    sum_len = 0
    while str_statements:
        sum_len += len(str_statements[0])
        temp_list.append(str_statements.pop(0))
        if sum_len > 3000:
            answers_list.append(temp_list)
            temp_list = []
            sum_len = 0
        elif len(str_statements) == 0:
            answers_list.append(temp_list)
    return answers_list

def wikify_message(message_str):
    """Function that transforms user message to wiki format"""
    return "_".join([y.capitalize() for y in message_str.split(" ")])

def unwikify_message(message_str):
    return "_".join([y for y in message_str.split(" ")])

def text_to_facts(message_str, multiple_choice=False):
    """Function that from string returns list of lists with facts
    Args:
        message_str (_str_): user input
        multiple_choice (bool, optional):
        Checks if there are multiple possible disambiguations for user input.
        Defaults to False.

    Returns:
        _type_: _description_
    """
    normal_str= unwikify_message(message_str)
    if len(normal_str.split()) >1:
        word_list = [normal_str.lower(), normal_str.capitalize(), 'The '+ normal_str.capitalize(), 'The ' + normal_str, normal_str+'s', "[Ii]t", ' '.join([i.capitalize() for i in normal_str.split(' ')])]
    else:
        word_list = [normal_str.lower(), normal_str.capitalize(), 'The '+ normal_str.capitalize(), 'The ' + normal_str, normal_str+'s', "[Ii]t"]

    if multiple_choice is False:
        unique_statements = list()
        wiki_doc = wiki_to_doc(str(message_str))
        for word in word_list:
            for cue in common_verbs:
                statements = custom_semistructured_statements(
                    doclike=wiki_doc,
                    entity=word,
                    cue=cue,
                    fragment_len_range=[0, 10_000],
                )
                for statement in statements:
                    unique_statements.append(statement)
    else:
        unique_statements = list()
        wiki_doc = wiki_to_doc(multiple_choice)
        for word in word_list:
            for cue in common_verbs:
                statements = custom_semistructured_statements(
                    doclike=wiki_doc,
                    entity=word,
                    cue=cue,
                    fragment_len_range=[0, 10_000],
                )
                for statement in statements:
                    unique_statements.append(statement)

    list_of_facts = answers(statements_to_sents(unique_statements))
    if not list_of_facts:
        list_of_facts = [
            ["Sorry, could not find anything interesting, maybe try another word?"]
        ]
    return list_of_facts


bot = telebot.TeleBot(BOT_TOKEN)


@bot.message_handler(commands=["start"])
def start(message):
    """Starts bot"""
    hi_user = f"Hello there {message.from_user.first_name}! \
        I can help you find some facts. To start just type the thing you're interested about"
    bot.send_message(message.chat.id, hi_user, parse_mode="html")


@bot.message_handler(commands=["help"])
def help_user(message):
    """Helping message to user"""
    help_user_text = "Please type thing you want to learn about in English:"
    bot.send_message(message.chat.id, help_user_text, parse_mode="html")


@bot.message_handler()
def get_user_text(message):
    """Function that takes message from user and returns to user a list of facts """
    message_str = wikify_message(str(message.text))
    page = wiki_wiki.page(message_str)
    dis_page = wiki_wiki.page(
        f"{message_str}_(disambiguation)"
    )  # check if user message has disambiguations
    if not dis_page.text:
        print(1)
        output = text_to_facts(message_str)
        for elem in output:
            bot_message = "\n".join(statement for statement in elem)
            bot.send_message(message.chat.id, bot_message, parse_mode="html")

    elif dis_page.text:
        page = dis_page
        p_links = [
            link.replace(" ", "_")
            for link in page.links
            if "(disambiguation)" not in link
            if "Talk:" not in link
            if "Help:" not in link
            if message.text or message.text.lower() in link
            if "Template" not in link
            if "Category:" not in link
            if "File:" not in link
        ]
        markup = telebot.types.InlineKeyboardMarkup()
        for link in p_links:
            markup.add(
                telebot.types.InlineKeyboardButton(
                    text=f"{link}", callback_data=f"r_{link}"
                )
            )
        bot.send_message(
            message.chat.id,
            "Which one do you want to learn about?",
            reply_markup=markup,
        )

        @bot.callback_query_handler(func=lambda call: "r_" in call.data)
        def query(call):
            bot.send_message(call.from_user.id, "Good choice!")
            bot.edit_message_reply_markup(
                call.from_user.id, message_id=call.message.message_id, reply_markup=""
            )
            bot.send_message(call.from_user.id, f"Started working on {call.data[2:]}")
            output = text_to_facts(message_str, multiple_choice=call.data[2:])
            for elem in output:
                bot_message = "\n".join(statement for statement in elem)
                bot.send_message(message.chat.id, bot_message,
                                 parse_mode="html")

    elif "may refer to" or "may also refer to":
        print(3)
        p_links = [
            link.replace(" ", "_")
            for link in page.links
            if "(disambiguation)" not in link
            if "Talk:" not in link
            if "Help:" not in link
            if message_str or message_str.lower() in link
            if "Template" not in link
            if "Category:" not in link
            if "File:" not in link
        ]
        markup = telebot.types.InlineKeyboardMarkup()
        print(p_links)
        for link in p_links:
            markup.add(
                telebot.types.InlineKeyboardButton(
                    text=f"{link}", callback_data=f"r_{link}"
                )
            )
        bot.send_message(
            message.chat.id,
            "Which one do you want to learn about?",
            reply_markup=markup,
        )

        @bot.callback_query_handler(func=lambda call: "r_" in call.data)
        def query(call):
            bot.send_message(call.from_user.id, "Good choice!")
            bot.edit_message_reply_markup(
                call.from_user.id, message_id=call.message.message_id, reply_markup=""
            )
            bot.send_message(call.from_user.id, "Started working")
            output = text_to_facts(message_str, multiple_choice=call.data[2:])
            for elem in output:
                bot_message = "\n".join(statement for statement in elem)
                bot.send_message(call.from_user.id, bot_message,
                                 parse_mode="html")


bot.polling(none_stop=True)


